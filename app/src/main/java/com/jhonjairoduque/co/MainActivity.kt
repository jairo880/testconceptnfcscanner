package com.jhonjairoduque.co

import DataContent
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Intent
import android.nfc.*
import android.nfc.tech.Ndef
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.UnsupportedEncodingException

/**
 * Created by jhonjairoduque on 18/05/2019.
 */

class MainActivity : AppCompatActivity() {

    // NFC adapter for checking NFC state in the device
    private var nfcAdapter: NfcAdapter? = null

    // Pending intent for NFC intent foreground dispatch.
    // Used to read all NDEF tags while the app is running in the foreground.
    private var nfcPendingIntent: PendingIntent? = null
    // Optional: filter NDEF tags this app receives through the pending intent.
    //private var nfcIntentFilters: Array<IntentFilter>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeNFCService()
        createPendingIntent()

        if (intent != null) {
            // Check if the app was started via an NDEF intent
            processResultDiscoveredIntent(intent)
        }

    }

    //region InitializeNFCService
    private fun initializeNFCService() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter == null) {
            Toast.makeText(this@MainActivity, getString(R.string.device_dont_support_nfc_service), Toast.LENGTH_SHORT)
                .show()
            finish()
        }

        if (!nfcAdapter?.isEnabled!!) {
            Toast.makeText(this@MainActivity, getString(R.string.enable_nfc_service), Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun createPendingIntent() {

        // Read all tags when app is running and in the foreground
        // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
        // will fill in the intent with the details of the discovered tag before delivering to
        // this activity.
        nfcPendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
    }

    //endregion

    private fun processResultDiscoveredIntent(checkIntent: Intent) {
        // Check if intent has the action of a discovered NFC tag
        // with NDEF formatted contents
        if (checkIntent.action == NfcAdapter.ACTION_NDEF_DISCOVERED) {
            val rawMessages = checkIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)

            if (rawMessages != null) {
                val messages = arrayOfNulls<NdefMessage?>(rawMessages.size)// Array<NdefMessage>(rawMessages.size, {})
                for (i in rawMessages.indices) {
                    messages[i] = rawMessages[i] as NdefMessage
                }
                // Process the messages array.
                processNdefMessages(messages)
            }

        }

        //write("Esto es una prueba", intent.getParcelableExtra(NfcAdapter.EXTRA_TAG))
    }

    private fun processNdefMessages(ndefMessages: Array<NdefMessage?>) {
        // Go through all NDEF messages found on the NFC tag
        for (currentMessage in ndefMessages) {
            if (currentMessage != null) {
                var message: String? = ""
                for (i in 0 until currentMessage.records.size) {
                    message += String(currentMessage.records[i].payload, Charsets.UTF_8)
                }
                text_view_messages.text = message!!.substring(3, message.length)
            }
        }
    }

    private fun processIntent(checkIntent: Intent) {
        // Check if intent has the action of a discovered NFC tag
        // with NDEF formatted contents
        if (checkIntent.action == NfcAdapter.ACTION_NDEF_DISCOVERED) {

            // Retrieve the raw NDEF message from the tag
            val rawMessages = checkIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)

            // Complete variant: parse NDEF messages
            if (rawMessages != null) {
                val messages = arrayOfNulls<NdefMessage?>(rawMessages.size)// Array<NdefMessage>(rawMessages.size, {})
                for (i in rawMessages.indices) {
                    messages[i] = rawMessages[i] as NdefMessage
                }
                // Process the messages array.
                processNdefMessages(messages)
            }

        } else {
            text_view_messages.text = getString(R.string.tittle_message_empty)
        }

    }


    private fun showDialogTagDiscovered(intent: Intent?) {
        var alertDialogBuilder = AlertDialog.Builder(this)

        alertDialogBuilder.setTitle(getString(R.string.tittle_alert))

        alertDialogBuilder.setPositiveButton(getString(R.string.tittle_read_act)) { _, _ ->
            intent?.let { processIntent(it) }
        }

        alertDialogBuilder.setNegativeButton(
            getString(R.string.tittle_write_act)
        ) { _, _ ->
            showDialogWriteTag(intent!!.getParcelableExtra(NfcAdapter.EXTRA_TAG))
        }
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.create().show()
    }

    private fun showDialogWriteTag(tag: Tag?) {
        var alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(getString(R.string.tittle_message_to_write))
        alertDialogBuilder.setCancelable(true)

        val inflater = this@MainActivity.layoutInflater
        val view = inflater.inflate(R.layout.alert_dialog_write_tag_view, null)

        alertDialogBuilder.setView(view)
3
        view.findViewById<EditText>(R.id.edit_text_content_to_write_tag).setText(DataContent.stringMessageToWrite)
        alertDialogBuilder.setPositiveButton(
            getString(R.string.tittle_write_act)
        ) { _, _ ->
            write(view.findViewById<EditText>(R.id.edit_text_content_to_write_tag).text.toString(), tag!!)
        }

        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.create().show()
    }

    private fun clearCurrentTextMessage() {
        text_view_messages.text = ""
    }

    @Throws(IOException::class, FormatException::class)
    private fun write(text: String, tag: Tag) {
        val records = arrayOf<NdefRecord>(createRecord(text))
        val message = NdefMessage(records)

        // Get an instance of Ndef for the tag.
        val ndef = Ndef.get(tag)
        // Enable I/O
        ndef.connect()
        // Write the message
        ndef.writeNdefMessage(message)
        // Close the connection
        ndef.close()

        Toast.makeText(this@MainActivity, getString(R.string.message_writed_successfully), Toast.LENGTH_SHORT).show()

        clearCurrentTextMessage()
    }

    @Throws(UnsupportedEncodingException::class)
    private fun createRecord(text: String): NdefRecord {
        val lang = "en"
        val textBytes = text.toByteArray()
        val langBytes = lang.toByteArray(charset("US-ASCII"))
        val langLength = langBytes.size
        val textLength = textBytes.size
        val payload = ByteArray(1 + langLength + textLength)

        // set status byte (see NDEF spec for actual bits)
        payload[0] = langLength.toByte()

        // copy langbytes and textbytes into payload
        System.arraycopy(langBytes, 0, payload, 1, langLength)
        System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength)

        return NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, ByteArray(0), payload)
    }

    //region LifeCycle
    override fun onResume() {
        super.onResume()
        // Get all NDEF discovered intents
        // Makes sure the app gets all discovered NDEF messages as long as it's in the foreground.
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null)
        // Alternative: only get specific HTTP NDEF intent
        //nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, nfcIntentFilters, null);
    }

    override fun onPause() {
        super.onPause()
        // Disable foreground dispatch, as this activity is no longer in the foreground
        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        // If we got an intent while the app is running, also check if it's a new NDEF message
        // that was discovered
        showDialogTagDiscovered(intent)
    }
//endregion

}
